#include <QApplication>
#include <QPushButton>
#include <QPixmap>
#include <iostream>
#include <QPainter>
#include <QPaintEvent>
#include <QTimer>
//
#include <QMediaPlayer>
#include <QMediaContent>

class ImageButton:public QPushButton{
Q_OBJECT
public:
    ImageButton()=default;
    ImageButton(QWidget *parent, QString dirApp);
    void paintEvent(QPaintEvent *e) override;
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;
    void keyPressEvent(QKeyEvent *e) override;
public slots:
    void setUp();
    void setDown();
private:
    QPixmap mCurrentButtonPixmap;
    QPixmap mButtonDownPixmap;
    QPixmap mButtonUpPixmap;
    bool isDown=false;
};

ImageButton::ImageButton(QWidget *parent, QString dirApp) {
    setParent(parent);
    setToolTip("Стоп");//подсказка
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    mButtonUpPixmap=QPixmap(dirApp+"/opt/button_up.png");
    mButtonDownPixmap=QPixmap(dirApp+"/opt/button_down.png");
    mCurrentButtonPixmap=mButtonUpPixmap;
    setGeometry(mCurrentButtonPixmap.rect());
    connect(this,&QPushButton::clicked,this,&ImageButton::setDown);
}

void ImageButton::paintEvent(QPaintEvent *e){
    QPainter p(this);
    p.drawPixmap(e->rect(),mCurrentButtonPixmap);
}

QSize ImageButton::sizeHint() const {
    return QSize(100,100);
}

QSize ImageButton::minimumSizeHint() const {
    return sizeHint();
}

void ImageButton::keyPressEvent(QKeyEvent *e) {
    setDown();
}

void ImageButton::setDown() {
    mCurrentButtonPixmap=mButtonDownPixmap;
    update();
    QTimer::singleShot(100,this,&ImageButton::setUp);
}

void ImageButton::setUp() {
    mCurrentButtonPixmap=mButtonUpPixmap;
    update();
}

int main(int argc, char **argv) {
    QApplication app(argc,argv);
    QWidget mainWindow;
    QString dirApp=app.applicationDirPath();
    ImageButton redButton(&mainWindow,dirApp);
    redButton.setFixedSize(200,200);
    mainWindow.move(1000,400);
    //
    auto *soundButton=new QMediaPlayer(&redButton);
    soundButton->setMedia(QUrl::fromLocalFile(dirApp+"/opt/sound.mp3"));
    soundButton->setVolume(75);

    QObject::connect(&redButton,&QPushButton::clicked,[&soundButton](){
        soundButton->stop();
        std::cout<<"clicked\n";
        soundButton->play();
    });

    mainWindow.show();
    return app.exec();
}

#include <main.moc>