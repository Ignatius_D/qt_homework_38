#include <iostream>
#include <QtGui/QtGui>
#include <QApplication>
//
#include <QVBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QLabel>
//
#include <QGraphicsScene>
#include <QGraphicsBlurEffect>
#include <QGraphicsPixmapItem>
//
#include <QFileDialog>
#include <QDir>

QImage blurImage(QImage source, int blurRadius){
    if(source.isNull()) {
        std::cerr<<"Don't find image"<<std::endl;
        return QImage();
    }
    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    item.setPixmap(QPixmap::fromImage(source));
    //
    auto *blur=new QGraphicsBlurEffect;
    blur->setBlurRadius(blurRadius);
    item.setGraphicsEffect(blur);
    scene.addItem(&item);
    QImage result(source.size(), QImage::Format_ARGB32);
    result.fill(Qt::transparent);
    QPainter painter(&result);
    scene.render(&painter,QRectF(),
                 QRectF(0,0,source.width(),source.height()));
    return result;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget mainWindow;
    QVBoxLayout vbox(&mainWindow);

    //Slider
    QSlider *blurSlider=new QSlider(&mainWindow);
    blurSlider->setOrientation(Qt::Horizontal);
    blurSlider->setMinimum(0);
    blurSlider->setMaximum(10);

    //Button
    auto *openFileButton=new QPushButton("Open",&mainWindow);

    //Image
    QString filePath;
    QLabel* labelImage=new QLabel(&mainWindow);
    labelImage->setAlignment(Qt::AlignCenter);
    QImage originalImage;
    QObject::connect(openFileButton,&QPushButton::clicked,[&filePath,&labelImage,&originalImage]()
    {
        filePath=QFileDialog::getOpenFileName(nullptr,
                                              "Open image",
                                              "/opt/",
                                              "Images (*.jpg *.png)");
        originalImage=QImage(filePath);
        labelImage->setPixmap(QPixmap::fromImage(blurImage(originalImage,0).scaled(
                originalImage.width(),
                originalImage.height(), Qt::KeepAspectRatio)));

    });

    QObject::connect(blurSlider,&QSlider::valueChanged,[&labelImage,&originalImage](qint64 value)
    {
        labelImage->setPixmap(QPixmap::fromImage(blurImage(originalImage,value).scaled(
                labelImage->width(),
                labelImage->height(), Qt::KeepAspectRatio)));
    });

    vbox.addWidget(labelImage);
    vbox.addWidget(openFileButton);
    vbox.addWidget(blurSlider);
    mainWindow.show();
    return a.exec();
}

//#include <main.moc>