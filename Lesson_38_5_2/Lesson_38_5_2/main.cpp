#include <QtGui/QtGui>
#include <QApplication>

#include <QWebEngineView>
#include <QPlainTextEdit>
#include <QGridLayout>
//#include <QSizePolicy>

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    //Main Window
    auto *MainWindow=new QWidget;
    MainWindow->resize(800, 600);

    QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(1);
    sizePolicy.setVerticalStretch(0);
    MainWindow->setSizePolicy(sizePolicy);

    auto *gridBox=new QGridLayout(MainWindow);

    //Text Editor
    auto *htmlTextEdit=new QPlainTextEdit;
    htmlTextEdit->setSizePolicy(sizePolicy);
    htmlTextEdit->setMinimumSize(QSize(100,100));


    //Web View
    auto *webView=new QWebEngineView;
    webView->setSizePolicy(sizePolicy);
    webView->setMinimumSize(QSize(100,100));

    gridBox->addWidget(htmlTextEdit, 0, 0, 1, 1);
    gridBox->addWidget(webView, 0, 1, 1, 1);

    QObject::connect(htmlTextEdit,&QPlainTextEdit::textChanged, [webView, htmlTextEdit](){
            webView->setHtml(htmlTextEdit->toPlainText());
            webView->show();
    });

    MainWindow->show();

    return a.exec();
}
